<?php require 'header.php'; ?>
<section class="block block--page">
    <div class="container">
        <div class="block__content">

            <h2 class="text text--sz-lg">Aviso de privacidad</h2>

            <div class="entry">

                <p>De acuerdo a lo Establecido en la “Ley Federal de Protección de Datos Personales en Posesión de los Particulares”, Diario Digitall S.A. de C.V. (“Business Insider México”) pone a su disposición el siguiente Aviso de Privacidad.</p>

                <p>Diario Digitall S.A. de C.V. (“Business Insider México”) es una empresa legalmente constituida de conformidad con las leyes mexicanas, con domicilio en Av. Santa Fe 270, Alcaldía Alvaro Obregón, CP 01210, CDMX; y como responsable del tratamiento de sus datos personales, hace de su conocimiento que los Datos Personales que recabamos de usted serán utilizados para las siguientes finalidades:</p>
                <p>Se utilizarán con el fin exclusivo de presentar, a manera informativa, la probabilidad de necesitar una vacuna contra la influenza. Así mismo, serán usados con fines de estadística siempre respetando su anonimato.</p>
                <p>Para llevar a cabo las finalidades descritas en el presente aviso de privacidad, utilizaremos los siguientes datos personales:</p>
                <p>Edad, Peso, Altura, Ciertas condiciones Médicas Preexistentes (Embarazo, Asma, Enfermedad Cardíaca, Diabetes, VIH/SIDA, Cancer, Enfermedad Renal Crónica), así como si el usuario de ha vacunado con anterioridad.</p>
                <p>Además de estos datos personales, se hará uso de los siguientes datos que conforme a la ley son considerados como datos personales sensibles, por lo cual requieren de su autorización expresa y por escrito para poder hacer uso de ellos. En este sentido, nos comprometemos con usted a no realizar ningún tipo de uso o tratamiento de sus datos personales sensibles, en tanto no se recabe la autorización firmada por escrito o a través de un sistema de firma electrónica, en el que usted nos otorgue expresamente el consentimiento para tratar los siguientes datos personales sensibles:</p>
                <p>Ciertas condiciones Médicas Preexistentes (Embarazo, Asma, Enfermedad Cardíaca, Diabetes, VIH/SIDA, Cancer, Enfermedad Renal Crónica)</p>
                <p>Para prevenir el acceso no autorizado a sus datos personales y con el fin de asegurar que la información sea utilizada para los fines establecidos en este aviso de privacidad, hemos establecido diversos procedimientos con la finalidad de evitar el uso o divulgación no autorizados de sus datos, permitiéndonos tratarlos debidamente.</p>
                <p>Así mismo, le informamos que sus datos personales pueden ser Transmitidos para ser tratados por personas distintas a esta empresa.</p>
                <p>Todos sus datos personales son tratados de acuerdo a la legislación aplicable y vigente en el país, por ello le informamos que usted tiene en todo momento los derechos (ARCO) de acceder, rectificar, cancelar u oponerse al tratamiento que le damos a sus datos personales; derecho que podrá hacer valer a través del Área Legal encargada de la seguridad de datos personales en el Teléfono (55) 5535007829, o por medio de su correo electrónico: <a href="mailto:jdelgado@businessinsider.mx">jdelgado@businessinsider.mx</a></p>
                <p>A través de estos canales usted podrá actualizar sus datos y especificar el medio por el cual desea recibir información, ya que en caso de no contar con esta especificación de su parte, Diario Digitall S.A. de C.V. (“Business Insider México”) establecerá libremente el canal que considere pertinente para enviarle información.</p>
                <p>Este aviso de privacidad podrá ser modificado por Diario Digitall S.A. de C.V. (“Business Insider México”), dichas modificaciones serán oportunamente publicadas a través de correo electrónico, teléfono, o cualquier otro medio de comunicación que Diario Digitall S.A. de C.V. (“Business Insider México”) determine para tal efecto.</p>
                <p>ATENTAMENTE<br/>
                Diario Digital S.A. de C.V. (“Business Insider México”)</p>
            </div>

        </div>
    </div>
    <!-- /container -->
</section>



<?php require 'footer.php'; ?>
