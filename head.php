<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="utf-8"/>

    <title>Flu Calculator — Vacunate contra la influenza</title>

    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="theme-color" content="#0065B0" />
    <link rel="stylesheet" media="all" href="css/screen.css?v=1" />

    <link rel="icon" type="image/png" href="https://onlines.com.ar/proyectos/campana-integral-influenza/images/favicon.ico" />
    <link rel="icon" type="image/png" sizes="32x32" href="images/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="images/favicon-16x16.png">

    <meta name="description" content="Vacunarse contra influenza es la mejor forma de cuidarte a ti y a los que más quieres.">
    <meta name="robots" content="index, follow">
    <link rel="canonical" href="">

    <meta property="og:title" content="Flu Calculator — Vacunate contra la influenza" />
    <meta property="og:type" content="article" />
    <meta property="og:url" content="" />
    <meta property="og:image" content="https://onlines.com.ar/proyectos/campana-integral-influenza/images/img-sharer.png" />
    <meta property="og:site_name" content=""/>
    <meta property="og:description" content="Vacunarse contra influenza es la mejor forma de cuidarte a ti y a los que más quieres." />
    <meta property="fb:admins" content="" />

    <meta name="twitter:card" content="summary_large_image">
    <meta name="twitter:site" content="@">
    <meta name="twitter:title" content="Flu Calculator — Vacunate contra la influenza">
    <meta name="twitter:description" content="">
    <meta name="twitter:image:src" content="https://onlines.com.ar/proyectos/campana-integral-influenza/images/img-sharer.png">

</head>
