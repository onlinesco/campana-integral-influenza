const message = document.getElementById('message');
const ageField = document.getElementById('input-age');
const weightField = document.getElementById('input-weight');
const heightField = document.getElementById('input-height');
const calculateButton = document.getElementById('calculate');

// IMC calculate
function imc(weight1, height1) {

    height = parseFloat(height1)/100;
    weight = parseFloat(weight1);

    var imc = weight / (height * height);
    var imc_value = 0;

    // Menos de 18.5 = 2
    // 18.5 – 24.9 = 1
    // 25.0 – 29.9 = 2
    // Menos de 18.5 = 3
    if (imc < 18.5) {
        imc_value = 2;
    } else if (imc >= 18.5 && imc <= 24.9) {
        imc_value = 1;
    } else if (imc > 25 && imc <= 29.9) {
        imc_value = 2;
    } else if (imc > 30) {
        imc_value = 3;
    }

    return imc_value;
}

// Array sum
function array_sum (array) {
    var key, sum = 0;

    // input sanitation
    if (typeof array !== 'object') {
        return null;
    }

    for (key in array) {
        //tester_print_r(typeof sum);
        sum += (array[key] * 1);
    }

    return sum;
}

// Risk calculate
function calculate(){

    // Age result
    // 0 a 64 años = 1
    // 65 o más años = 2
    var age = ageField.value;
    if(age) {
        if(age < 64) {
            age_result = 1;
        } else if( age >= 65) {
            age_result = 2;
        } else {
            age_result = false;
        }
    }

    // Altura y peso
    var weight = weightField.value;
    var height = heightField.value;

    // IMC result
    var imc_result = imc(weight, height);

    // Conditions
    var conditions_array = []
    var conditions = document.querySelectorAll('input[type=checkbox]:checked')

    // Array of conditions
    for (var i = 0; i < conditions.length; i++) {
        parseFloat(conditions[i]);
        conditions_array.push(conditions[i].value)
    }

    var conditions_result = array_sum(conditions_array);

    // Vaccinated
    var vaccinated_result = document.querySelector('input[name=data-vaccinated]:checked').value;
    vaccinated_result = parseFloat(vaccinated_result);

    console.log('Resultado de edad:'+age_result);
    console.log(weight);
    console.log(height);
    console.log('Resultado de IMC:'+imc_result);
    console.log('Resultado de condiciones:'+conditions_result);
    console.log('Resultado de vacunación:'+vaccinated_result);

    var sum_total = age_result + imc_result + conditions_result + vaccinated_result;

    console.log('Suma total:'+sum_total);
    if(sum_total < 4) {
        message.innerHTML = '<div class="box__header">'+
            '<span class="headline">Si contraes influenza tienes</span>'+
            '<h3 class="title title--sz-lg">Riesgo bajo</h3>'+
        '</div>'+
        '<p class="text-2 text-2--sz-md text-2--m-bottom">Se recomienda que todas las personas a partir de los 6 meses de vida se vacunen contra la influenza <strong>todos los años</strong>. ¡Es la mejor opción para protegerte y a tus seres queridos! #porlavacunacion</p>';
    } else {
        message.innerHTML = '<div class="box__header">'+
            '<span class="headline">Si contraes influenza tienes</span>'+
            '<h3 class="title title--sz-lg">Riesgo alto</h3>'+
        '</div>'+
        '<p class="text-2 text-2--sz-md text-2--m-bottom">Tienes mayor riesgo de presentar <strong>complicaciones graves</strong> causadas por la influenza. ¡La mejor opción para protegerte y proteger a tus seres queridos es la vacunación!</p>';
    }
    

}

function validateField (field, errorMessage) {
    // Check if field is empty
    if(field.value == "") {
        field.parentNode.classList.add('input--error');
        field.parentNode.lastElementChild.innerHTML = errorMessage;
    } else {
        field.parentNode.classList.remove('input--error');
        field.parentNode.lastElementChild.innerHTML = '';
    }
}

calculateButton.addEventListener('click', function (event) {

    validateField(ageField, 'Debes completar tu edad');
    validateField(weightField, 'Debes completar tu peso');
    validateField(heightField, 'Debes completar tu altura');

    // Si ninguno esta vacio ejecuto la funcion
    if(ageField.value != "" && weightField.value != "" && heightField.value != "") {
        calculate();
        window.location.href = "#message";
    }
});