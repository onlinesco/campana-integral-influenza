        <footer class="footer">
            <div class="container">
                <a href="https://onlines.com.ar/proyectos/campana-integral-influenza/" class="footer__logo"><img src="images/logo.svg" alt="Flu Calculator"></a>
                <span class="footer__copy">Este sitio no constituye un diagnóstico, solo es informativo. Consulta a tu médico. <a href="https://espanol.cdc.gov/flu/highrisk" target="_blank" rel="nofollow">Fuentes</a>. <a href="https://onlines.com.ar/proyectos/campana-integral-influenza/aviso-de-privacidad.php" target="_blank" rel="nofollow">Aviso de privacidad.</a></span>
            </div>
            <!-- /container -->
        </footer>
        
        <script src="vendor/js/jquery-3.2.1.min.js"></script>
        <script type="text/javascript" src="vendor/js/jquery.fancybox.min.js"></script>
        <script src="js/app.min.js"></script>
    </body>
</html>
