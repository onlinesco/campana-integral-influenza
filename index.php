<?php require 'header.php'; ?>

<div class="bg-stripe"><div class="stripe__img"><div class="stripe__bg" style="background-image: url(images/bg-virus.png);"></div></div></div>

<section class="block block--form block--p-top">
    <div class="container">
        <div class="grid grid--7y5">
            <div class="grid__content">
                <h2 class="text text--sz-lg">Vacunarse contra influenza es la mejor forma de <strong>cuidarte a ti y a los que más quieres.</strong></h2>
                <div id="message" class="box box--bg-white box--border box--rounded box--before box--pad">
                    
                    <div class="box__header">
                        <h3 class="title">Conoce tu riesgo ahora</h3>
                        <span class="headline">Completa tus datos</span>
                    </div>
                    <!--/box-header-->

                    <div class="form">
                        <div class="form__grid">
                            <div class="input input--horiz input--num">
                                <label class="input__label" for="input-age">Edad</label>
                                <div class="input__box">
                                    <input name="data-age" id="input-age" type="number" min="1" required>
                                    <div class="icon-box">
                                        <i class="icon-angle-up"></i>
                                        <i class="icon-angle-down"></i>
                                    </div>
                                    <!-- /icon-box -->
                                    <span class="btn__text"></span>
                                </div>
                                <!-- /input__box -->
                            </div>
                            <!-- /input -->
                            <div class="input input--horiz input--num">
                                <label class="input__label" for="input-weight">Peso</label>
                                <div class="input__box">
                                    <span class="input__text">kg.</span>
                                    <input name="data-weight" id="input-weight" type="number" min="1" max="600" required>
                                    <div class="icon-box">
                                        <i class="icon-angle-up"></i>
                                        <i class="icon-angle-down"></i>
                                    </div>
                                    <!-- /icon-box -->
                                    <span class="btn__text"></span>
                                </div>
                                <!-- /input__box -->
                            </div>
                            <!-- /input -->
                            <div class="input input--horiz input--num">
                                <label class="input__label" for="input-height">Altura</label>
                                <div class="input__box">
                                    <span class="input__text">cms.</span>
                                    <input name="data-height" id="input-height" type="number" min="1" max="260" required>
                                    <div class="icon-box">
                                        <i class="icon-angle-up"></i>
                                        <i class="icon-angle-down"></i>
                                    </div>
                                    <!-- /icon-box -->
                                    <span class="btn__text"></span>
                                </div>
                                <!-- /input__box -->
                            </div>
                            <!-- /input -->
                        </div>
                        <!-- /form__grid -->
                        <div class="input input--horiz input--checkbox">
                            <span class="input__label">Condiciones</span>
                            <ul class="input__list">
                                <li>
                                    <label class="checkbox" for="input-embarazo">
                                        <input type="checkbox" value="2" name="data-conditions[]" id="input-embarazo">
                                        <span class="checkmark"><span class="checkmark__check"></span></span>
                                        <span class="form__text">Embarazo</span>
                                    </label>
                                </li>
                                <li>
                                    <label class="checkbox" for="input-asma">
                                        <input type="checkbox" value="2" name="data-conditions[]" id="input-asma">
                                        <span class="checkmark"><span class="checkmark__check"></span></span>
                                        <span class="form__text">Asma</span>
                                    </label>
                                </li>
                                <li>
                                    <label class="checkbox" for="input-enfermedad-cardiaca">
                                        <input type="checkbox" value="2" name="data-conditions[]" id="input-enfermedad-cardiaca">
                                        <span class="checkmark"><span class="checkmark__check"></span></span>
                                        <span class="form__text">Enfermedad cardíaca</span>
                                    </label>
                                </li>
                                <li>
                                    <label class="checkbox" for="input-diabetes">
                                        <input type="checkbox" value="2" name="data-conditions[]" id="input-diabetes">
                                        <span class="checkmark"><span class="checkmark__check"></span></span>
                                        <span class="form__text">Diabetes</span>
                                    </label>
                                </li>
                                <li>
                                    <label class="checkbox" for="input-sida">
                                        <input type="checkbox" value="3" name="data-conditions[]" id="input-sida">
                                        <span class="checkmark"><span class="checkmark__check"></span></span>
                                        <span class="form__text">VIH/Sida</span>
                                    </label>
                                </li>
                                <li>
                                    <label class="checkbox" for="input-cancer">
                                        <input type="checkbox" value="3" name="data-conditions[]" id="input-cancer">
                                        <span class="checkmark"><span class="checkmark__check"></span></span>
                                        <span class="form__text">Cáncer</span>
                                    </label>
                                </li>
                                <li>
                                    <label class="checkbox" for="input-renal">
                                        <input type="checkbox" value="2" name="data-conditions[]" id="input-renal">
                                        <span class="checkmark"><span class="checkmark__check"></span></span>
                                        <span class="form__text">Enfermedad renal crónica</span>
                                    </label>
                                </li>
                            </ul>
                        </div>
                        <!-- /input -->
                        <div class="input input--horiz input--radio-button">
                            <span class="input__label">¿Te has vacunado contra la influenza?</span>
                            <div class="radio__options">
                                <label class="radio-button" for="input-vaccinated-no">
                                    <input type="radio" value="3" name="data-vaccinated" id="input-vaccinated-no" checked>
                                    <span class="checkmark"><span class="form__text">No</span></span>
                                </label>
                                <label class="radio-button" for="input-vaccinated-si">
                                    <input type="radio" value="1" name="data-vaccinated" id="input-vaccinated-si">
                                    <span class="checkmark"><span class="form__text">Si</span></span>
                                </label>
                            </div>
                            <!-- /radio__options -->
                        </div>
                        <!-- /input -->
                        <div class="btn-box btn-box--pad">
                            <button class="btn-bg" id="calculate">Enviar</button>
                            <span class="btn__text">Enviar el formulario para conocer tu riesgo</span>
                        </div>
                        <!-- /btn-box -->
                    </div>

                </div>
                <!-- /box -->
            </div>
            <!-- /grid__content -->
            <div class="sidebar-sticky">
                <img src="images/dr.png" alt="Doctora animada" class="img-sticky">
            </div>
            <!-- /sidebar-sticky -->
        </div>
        <!-- /grid-7y5 -->
    </div>
    <!-- /container -->
</section>

<section class="block block--title block--pad">
    <div class="container">
        <div class="logo">
            <img src="images/logo-virus-white.svg" alt="Influenza">
            <h2 class="logo__title">Influenza</h2>
            <h3 class="headline">Conoce más sobre el virus</h3>
        </div>
    </div>
    <!-- /container -->
</section>

<div class="block block--video">
    <div class="container">
        <div class="grid grid--7y5">
            <a href="https://www.youtube.com/watch?v=JmRvZvrUtNg" data-fancybox class="box box--bg-black box--border box--rounded box--before box--video" style="background-image: url(images/video-thumb.png);"><i class="icon-play-circle"></i></a>
            <!-- /box -->
            <div class="block__sidebar">
                <img src="images/dr-face.png" alt="Cara de la doctora animada">
                <p class="text">Si te cuidas tú, <strong>nos cuidamos todos.</strong></p>
            </div>
            <!-- /block__sidebar -->
        </div>
        <!-- /grid-7y5 -->
    </div>
    <!-- /container -->
</div>
<!-- /block -->

<div class="block block--faqs block--p-top-lg">
    <div class="container">
        <div class="grid grid--2">
            
            <div class="faq">
                <h4 class="faq__title"><span>¿Sabías qué?</span></h4>
                <a href="images/infografia.jpg" data-fancybox="info" class="box box--bg-white box--border box--lightbox"><div style="background-image: url(images/img-link.png);"></div></a>
            </div>
            <!-- /box -->
            
            <div class="faq">
                <h4 class="faq__title"><span>Consejos útiles</span></h4>
                <a href="images/infografia-2.jpg" data-fancybox="info" class="box box--bg-white box--border box--lightbox"><div style="background-image: url(images/img-link-2.png);"></div></a>
            </div>
            <!-- /box -->
            <div class="faq">
                <h4 class="faq__title"><span>¿Qué debo saber acerca de la influenza?</span></h4>
                <div class="faq__extract">
                    <p>La influenza es una enfermedad respiratoria contagiosa provocada por los virus de la influenza. Puede causar una enfermedad leve a grave. Los resultados graves de la infección por influenza pueden ser la hospitalización o la muerte. Algunas personas como las personas mayores, niños pequeños y las personas con ciertas afecciones corren un alto riesgo de presentar graves complicaciones por la influenza. Existen dos tipos de virus (gripe): A y B. Los virus de influenza A y B que generalmente se diseminan entre las personas (virus de influenza humana) todos los años causan epidemias de influenza estacional.</p>
                    <strong>La mejor manera de prevenir la influenza es vacunarse todos los años.</strong>
                </div>
                <!-- /faq__extract -->
            </div>
            <!-- /box -->
            <div class="faq">
                <h4 class="faq__title"><span>¿Cuáles son los síntomas de la influenza?</span></h4>
                <div class="faq__extract">
                    <p>Los signos y síntomas de la influenza suelen aparecer de manera repentina. Las personas enfermas a causa del virus la influenza a menudo tienen algunos de estos síntomas o todos:</p>
                    <ul>
                        <li>Fiebre o calentura</li>
                        <li>Ecalofríos</li>
                        <li>Tos</li>
                        <li>Dolor de garganta</li>
                        <li>Mucosidad nasal o nariz tapada</li>
                        <li>Dolores musculares y corporales</li>
                        <li>Dolores de cabeza</li>
                        <li>Fatiga (cansancio)</li>
                        <li>Algunas personas pueden tener vómitos y diarrea, aunque esto es más común en los niños que en los adultos.</li>
                    </ul>
                </div>
                <!-- /faq__extract -->
            </div>
            <!-- /box -->
            <div class="faq">
                <h4 class="faq__title"><span>¿Quiénes son más propensos a contraer la influenza?</span></h4>
                <div class="faq__extract">
                    <p>Todos nos podemos contagiar de <strong>influenza</strong>, sin embargo, hay ciertos grupos con mayor riesgo de desarrollar complicaciones, por ejemplo, los adultos mayores, personas con obesidad, pacientes con tratamientos contra el cáncer o alguna inmunodeficiencia. </p>
                    <ul>
                        <li>Niños de 6-59 meses</li>
                        <li>Adultos mayores de 60 años</li>
                        <li>Embarazo</li>
                        <li>Diabetes</li>
                        <li>Asma</li>
                        <li>Obesidad</li>
                        <li>Alguna Inmunodeficiencia (VIH , cáncer, tratamientos inmunosupresores) </li>
                        <li>Fatiga (cansancio)</li>
                        <li>Enfermedad cardiovascular  (cardiopatía isquémica, hipertensión arterial)</li>
                    </ul>
                </div>
                <!-- /faq__extract -->
            </div>
            <!-- /box -->
        </div>
        <!-- /grid-7y5 -->
    </div>
    <!-- /container -->
</div>
<!-- /block -->

<?php require 'footer.php'; ?>
